if grep -q -F 'exclude' /etc/dnf/dnf.conf ; then sed 's/^exclude=.*/& gnome-tour/' /etc/dnf/dnf.conf; else echo exclude=gnome-tour >> /etc/dnf/dnf.conf; fi

dnf -y install \
    @base-x \
    gnome-shell \
    gnome-terminal \
    podman \
    distrobox \
    nautilus \
    f$(rpm -E %fedora)-backgrounds-gnome \
    f$(rpm -E %fedora)-backgrounds-extras-gnome \
    fedora-workstation-backgrounds \
    gnome-backgrounds \
    desktop-backgrounds-gnome \
    gnome-terminal-nautilus \
    xdg-user-dirs \
    xdg-user-dirs-gtk \
    plymouth-theme-spinner \
    unzip

systemctl set-default graphical.target
plymouth-set-default-theme spinner -R
grub2-editenv - set menu_auto_hide=1
flatpak remote-delete fedora
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub \
    org.mozilla.firefox \
    flathub tv.kodi.Kodi \
    org.freedesktop.Platform.ffmpeg-full \
    io.github.TransmissionRemoteGtk \
    org.libreoffice.LibreOffice \
    com.mattjakeman.ExtensionManager \
    org.fedoraproject.MediaWriter \
    com.belmoussaoui.Authenticator \
    com.github.micahflee.torbrowser-launcher \
    ch.protonmail.protonmail-bridge \
    com.vscodium.codium \
    io.mpv.Mpv \
    org.mozilla.Thunderbird \
    im.riot.Riot \
    com.valvesoftware.Steam \
    org.gnome.font-viewer \
    org.gnome.eog \
    org.gnome.Calculator \
    org.gnome.Characters \
    org.gnome.TextEditor \
    com.jetbrains.PyCharm-Professional \
    com.jetbrains.IntelliJ-IDEA-Ultimate \
    com.jetbrains.GoLand \
    org.gtk.Gtk3theme.Adwaita-dark

HOSTNAME=`hostname -s`           
if [ "$HOSTNAME" == "ws02" ]; then
    grubby --update-kernel=ALL --args="module_blacklist=hid_sensor_hub"
fi
